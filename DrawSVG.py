import svgwrite
from svgwrite import cm, mm
import MapResources
import Consts

def drawContinentMap(hex_map, filename="continent_map.svg"):
	dwg = svgwrite.Drawing(filename=filename)
	for line in hex_map:
		for h in line:
			addHexToDrawing(dwg, h, 2)
	dwg.save()

def addHexToDrawing(dwg, h, scale):
	if(h.grid_x % 2 == 0):
		dwg.add(dwg.polygon(
			[
				[scale*((Consts.HexWidth + Consts.HexDepth) * h.grid_x + Consts.HexDepth), scale*(Consts.HexHeight * h.grid_y)],
				[scale*((Consts.HexWidth + Consts.HexDepth) * h.grid_x + Consts.HexWidth + Consts.HexDepth), scale*(Consts.HexHeight * h.grid_y)],
				[scale*((Consts.HexWidth + Consts.HexDepth) * h.grid_x + Consts.HexWidth + Consts.HexWidth), scale*(Consts.HexHeight * h.grid_y + Consts.HexOffset)],
				[scale*((Consts.HexWidth + Consts.HexDepth) * h.grid_x + Consts.HexWidth + Consts.HexDepth), scale*(Consts.HexHeight * h.grid_y + Consts.HexHeight)],
				[scale*((Consts.HexWidth + Consts.HexDepth) * h.grid_x + Consts.HexDepth), scale*(Consts.HexHeight * h.grid_y + Consts.HexHeight)],
				[scale*((Consts.HexWidth + Consts.HexDepth) * h.grid_x), scale*(Consts.HexHeight * h.grid_y + Consts.HexOffset)]
			], fill=Consts.BiomeColors[h.biome], stroke='black'))
	else:
		dwg.add(dwg.polygon(
			[
				[scale*((Consts.HexWidth + Consts.HexDepth) * h.grid_x + Consts.HexDepth), scale*(Consts.HexHeight * h.grid_y + Consts.HexOffset)],
				[scale*((Consts.HexWidth + Consts.HexDepth) * h.grid_x + Consts.HexWidth + Consts.HexDepth), scale*(Consts.HexHeight * h.grid_y + Consts.HexOffset)],
				[scale*((Consts.HexWidth + Consts.HexDepth) * h.grid_x + Consts.HexWidth + Consts.HexWidth), scale*(Consts.HexHeight * h.grid_y + Consts.HexOffset + Consts.HexOffset)],
				[scale*((Consts.HexWidth + Consts.HexDepth) * h.grid_x + Consts.HexWidth + Consts.HexDepth), scale*(Consts.HexHeight * h.grid_y + Consts.HexHeight + Consts.HexOffset)],
				[scale*((Consts.HexWidth + Consts.HexDepth) * h.grid_x + Consts.HexDepth), scale*(Consts.HexHeight * h.grid_y + Consts.HexHeight + Consts.HexOffset)],
				[scale*((Consts.HexWidth + Consts.HexDepth) * h.grid_x), scale*(Consts.HexHeight * h.grid_y + Consts.HexOffset + Consts.HexOffset)]
			], fill=Consts.BiomeColors[h.biome], stroke='black'))


drawContinentMap(MapResources.createContinentMap())