#This file contains all resources for mapping

from HexResources import Hex
import Consts
#Edges follows NEWS ordering
def createContinentMap(x=25,y=25,edges=['mountain','ocean','ocean','ocean'],heat=['freezing','cold','hot','blazing']):
	retMap = []
	i = 0
	while(i < y):
		j = 0
		retMap += [[]] 
		while(j < x):
			if i == 0:
				retMap[i] += [Hex(j, i, biome=edges[0])]
			elif i == y-1:
				retMap[i] += [Hex(j, i, biome=edges[3])]
			elif j == 0:
				retMap[i] += [Hex(j, i, biome=edges[1])]
			elif j == x-1:
				retMap[i] += [Hex(j, i, biome=edges[2])]
			else:
				retMap[i] += [Hex(j, i)]
			j += 1
		i += 1
	return retMap