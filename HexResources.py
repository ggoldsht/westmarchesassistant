#This file contains all resources for hexes

import Consts

class Hex:
	grid_x = -1
	grid_y = -1
	scale = -1
	biome = 'empty'
	#Neighbors are lsited in N(NE)(NW)(SE)(SW)S order
	def get_neighbors(self):
		if(self.grid_x % 2 == 0):
			return [(grid_x, grid_y-1),(grid_x+1, grid_y-1),(grid_x-1, grid_y-1),(grid_x+1, grid_y),(grid_x-1, grid_y),(grid_x, grid_y+1)]
		else:
			return [(grid_x, grid_y-1),(grid_x+1, grid_y),(grid_x-1, grid_y),(grid_x+1, grid_y+1),(grid_x-1, grid_y+1),(grid_x, grid_y+1)]

	def __init__(self, grid_x=-1, grid_y=-1,scale=-1,biome='empty'):
		self.grid_x = grid_x
		self.grid_y = grid_y
		self.scale = scale
		self.biome = biome
	def __str__(self):
		return "X: " + str(self.grid_x) + ", Y: " + str(self.grid_y) + ", Biome: " + self.biome