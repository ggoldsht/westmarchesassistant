#This file contains constants for other files

HexHeight = 10
HexOffset = HexHeight/2
HexWidth = 5
HexDepth = 2.5

BiomeTypes = ['empty', 'water', 'ocean', 'plains', 'forest', 'desert', 'mountain']
BiomeColors = {'empty' : 'black', 'water' : 'blue', 'ocean' : 'blue', 'plains' : 'light_green', 'forest' : 'green', 'desert' : 'yellow', 'mountain' : 'grey'}
TemperatureThesholds = [[0, 30, 'freezing'],[30, 50, 'cold'],[50, 70, 'temperate'],[70, 90, 'hot'],[90, 120, 'blazing']]
BiomeNames = {
	('empty', 'freezing') : 'empty',
	('empty', 'cold') : 'empty',
	('empty', 'temperate') : 'empty',
	('empty', 'hot') : 'empty',
	('empty', 'blazing') : 'empty',
	('water', 'freezing') : 'glacier',
	('water', 'cold') : 'cold water',
	('water', 'temperate') : 'water',
	('water', 'hot') : 'warm water',
	('water', 'blazing') : 'steamy water',
	('ocean', 'freezing') : 'salt glacier',
	('ocean', 'cold') : 'cold sea',
	('ocean', 'temperate') : 'sea',
	('ocean', 'hot') : 'warm sea',
	('ocean', 'blazing') : 'boiling sea',
	('plains', 'freezing') : 'tundra',
	('plains', 'cold') : 'steppe',
	('plains', 'temperate') : 'plains',
	('plains', 'hot') : 'savanna',
	('plains', 'blazing') : 'fire plains',
	('forest', 'freezing') : 'snow forest',
	('forest', 'cold') : 'pine forest',
	('forest', 'temperate') : 'forest',
	('forest', 'hot') : 'tropical forest',
	('forest', 'blazing') : 'rain forest',
	('desert', 'freezing') : 'frost desert',
	('desert', 'cold') : 'cold desert',
	('desert', 'temperate') : 'badlands',
	('desert', 'hot') : 'desert',
	('desert', 'blazing') : 'hell desert',
	('mountain', 'freezing') : 'snowy mountain',
	('mountain', 'cold') : 'mountain',
	('mountain', 'temperate') : 'wooded mountain',
	('mountain', 'hot') : 'barren mountain',
	('mountain', 'blazing') : 'scortched mountain'
}
